# Technical interview

You'll first complete a small algorithmic exercise, a test suite is provided to validate your code.

Then you'll expose the logic as an HTTP endpoint.

## Goal

There is a rectangle of given width __w__ and height __h__,

On the width side, you are given a list of measurements.
On the height side, you are given another list of measurements.

Draw perpendicular lines from the measurements to partition the rectangle into smaller rectangles.

In all sub-rectangles (include the combinations of smaller rectangles), how many of them are squares?


### Example

w = 10

h = 5

measurements on x-axis: 2, 5

measurements on y-axis: 3

```
  0___2______5__________10 
  |   |      |          |
  |   |      |          |
 3|___|______|__________|
  |   |      |          |
 5|___|______|__________|

```

Number of squares in sub-rectangles = 4 (one 2x2, one 3x3, two 5x5)


## Input

Line 1: Integers w h countX countY, separated by space

Line 2: list of measurements on the width side, countX integers separated by space, sorted in asc order

Line 3: list of measurements on the height side, countY integers separated by space, sorted in asc order

Example input :

```
10 5 2 1
2 5
3
```
## Output

The number of squares in sub-rectangles created by the added lines.

### Constraints

1 ≤ w, h ≤ 20,000

1 ≤ number of measurements on each axis ≤ 500
