import { test } from 'node:test'
import { readFileSync } from 'node:fs'
import { ok, strictEqual } from 'node:assert'
import { resolve } from 'node:path'

import { main } from './main'

test('Sample', async () => {
  const buffer = readFileSync(resolve(__dirname, '../../samples/01.txt'))
  const output = main(buffer.toString())
  strictEqual(output, 4)
})

test('Squarish', async () => {
  const buffer = readFileSync(resolve(__dirname, '../../samples/02.txt'))
  const output = main(buffer.toString())
  strictEqual(output, 14)
})

test('Bigger 1', async () => {
  const buffer = readFileSync(resolve(__dirname, '../../samples/03.txt'))
  const output = main(buffer.toString())
  strictEqual(output, 123)
})

test('Bigger 2', async () => {
  const buffer = readFileSync(resolve(__dirname, '../../samples/04.txt'))
  const output = main(buffer.toString())
  strictEqual(output, 36)
})

test('Lo-density 1', async () => {
  const buffer = readFileSync(resolve(__dirname, '../../samples/05.txt'))
  const output = main(buffer.toString())
  strictEqual(output, 25)
})

test('Lo-density 2', async () => {
  const buffer = readFileSync(resolve(__dirname, '../../samples/06.txt'))
  const output = main(buffer.toString())
  strictEqual(output, 0)
})

// Test perf on larger sets by unksipping them

test('Hi-density 1', { skip: true }, async () => {
  const buffer = readFileSync(resolve(__dirname, '../../samples/07.txt'))
  const output = main(buffer.toString())
  strictEqual(output, 22281)
})

test('Hi-density 2', { skip: true }, async () => {
  const buffer = readFileSync(resolve(__dirname, '../../samples/08.txt'))
  const output = main(buffer.toString())
  strictEqual(output, 18431)
})

test('Imbalance', { skip: true }, async () => {
  const buffer = readFileSync(resolve(__dirname, '../../samples/09.txt'))
  const output = main(buffer.toString())
  strictEqual(output, 42)
})
