import test, { after, before, describe } from 'node:test'
import { ok, strictEqual } from 'node:assert'
import { Server } from 'node:http'

describe('api', { skip: true }, () => {
  const BASE_URL = 'http://localhost:3000'
  let server: Server

  before(async () => {
    server = (await import('./api.js')).server
    await new Promise((res) => server.once('listening', res))
  })
  after(async () => {
    await new Promise((res) => server.close())
  })

  test('GET /hello respond with 200', async () => {
    const res = await fetch(`${BASE_URL}/hello`, {
      method: 'GET',
    })
    strictEqual(res.status, 200)
    const body = await res.json()
    ok(typeof body === 'object' && body != null && 'greeting' in body)
    strictEqual(body.greeting, 'Hello world!')
  })
})
