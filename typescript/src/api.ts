import express from 'express'

const app = express()
const port = 3000

app.get('/hello', (req, res) => {
  res.status(200).json({ greeting: 'Hello World!' })
})

// TODO step 2 add a route to expose the main function

const server = app.listen(port, () => console.log('Listening on', port))

export { server }
